const Contact = () => {
  return (
    <section className="py-20">
      <div className="container">
        <div>
          <h2 className="mb-8 text-4xl text-center">
            Get in contact with our data experts
          </h2>
        </div>
        <form className="bg-purple-light p-5 rounded-xl md:p-10">
          <div className="grid md:grid-cols-2 gap-4">
            <input
              type="text"
              className="px-4 py-3 bg-input border border-gray-300 rounded-lg text-lg focus:outline-none focus:border-input-focus transition-300"
              placeholder="Name"
              required
            />
            <input
              type="text"
              className="px-4 py-3 bg-input border border-gray-300 rounded-lg text-lg focus:outline-none focus:border-input-focus transition-300"
              placeholder="Phone Number"
              required
            />
          </div>
          <div className="mt-4 grid md:grid-cols-2 gap-4">
            <input
              type="email"
              className="px-4 py-3 bg-input border border-gray-300 rounded-lg text-lg focus:outline-none focus:border-input-focus transition-300"
              placeholder="Email"
              required
            />
            <input
              type="text"
              className="px-4 py-3 bg-input border border-gray-300 rounded-lg text-lg focus:outline-none focus:border-input-focus transition-300"
              placeholder="Company name (optional)"
            />
          </div>
          <div className="mt-4">
            <textarea
              rows="4"
              placeholder="Your message"
              className="px-4 py-3 bg-input border border-gray-300 rounded-lg text-lg w-full focus:outline-none focus:border-input-focus transition-300"
            />
          </div>
          <div className="mt-4 grid md:grid-cols-2 md:gap-4">
            <button
              type="submit"
              className="md:col-start-2 w-full px-4 py-3 rounded-lg text-lg bg-purple text-white hover:bg-purple-dark focus:outline-none focus:bg-purple-dark transition-300"
            >
              Send
            </button>
          </div>
        </form>
      </div>
    </section>
  );
};

export default Contact;
