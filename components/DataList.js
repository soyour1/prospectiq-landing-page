const DataList = () => {
  return (
    <section className="bg-3">
      <div className="container text-center py-4">
        <h1 className="text-3xl md:text-4xl pb-2">
          How we build your perfect data list
        </h1>
        <p className="my-4 font-montserrat text-gray-400 md:text-xl md:leading-loose md:px-40 lg:px-52">
          We are committed to supporting our clients develop performing
          marketing campaigns and grow their business by utilising quality and
          accurate B2B data
        </p>
      </div>
    </section>
  );
};

export default DataList;
