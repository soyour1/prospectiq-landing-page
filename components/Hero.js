import Image from "next/image";

const Hero = () => {
  return (
    <section className="mt-24">
      <div className="container grid grid-cols-1 gap-2 md:grid-cols-2">
        <div className="md:order-2">
          <Image
            src="/images/hero.svg"
            width="705"
            height="743"
            layout="responsive"
          />
        </div>
        <div className="md:p-5 flex justify-center items-center md:order-1">
          <div className="text-center md:text-left">
            <h1 className="text-4xl my-10 md:mt-0 md:mb-7 md:text-5xl">
              Quality B2B contact data to help you reach your next customers
            </h1>
            <p className="my-4 mb-7 font-montserrat text-lg text-gray-500 font-light leading-loose">
              Access over 3.5 million decision makers across the UK which you
              can contact via <strong className="font-semibold">Email</strong>,{" "}
              <strong className="font-semibold">Telephone</strong> and{" "}
              <strong className="font-semibold">Direct Mail</strong>
            </p>
            <div className="flex flex-col space-y-3 items-center md:flex-row md:space-x-3 md:space-y-0">
              <button className="px-8 py-4 border border-gray-400 rounded-lg text-gray-500 hover:bg-purple hover:text-white focus:bg-purple focus:text-white focus:outline-none active:outline-none transition-300">
                Buy UK B2B Data
              </button>
              <button className="px-5 py-4 border bg-purple text-white rounded-lg hover:border-gray-400 hover:text-gray-500 hover:bg-white focus:border-gray-400 focus:bg-white focus:text-gray-500 focus:outline-none active:outline-none transition-300">
                Book a discovery call
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
